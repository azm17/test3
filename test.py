# -*- coding: utf-8 -*-
import sympy


epsilon = 0#.25
xi = 1#0.5
print('epsilon',epsilon,'xi',xi)
L = 0.5
G = 0.5

RE = 1 - (L + 1)*(epsilon + xi)
SE = -L + (1 + L)*(epsilon + xi)
TE = (1 + G)*(1 - epsilon - xi)
PE = (1 + G)*(epsilon + xi)
print('RE SE TE PE\n',RE,SE,TE,PE)
mu = 1- epsilon-xi
eta = xi + epsilon
for i in range(1):
    for j in range(1):
        
        p4 = 1/3#i*0.01
        p1 =2/3#j*0.01
        #sy=0.5
        #p1 = sympy.Symbol('p1')
        sy = sympy.Symbol('sy')
        
        #p1 = sympy.Symbol('p1')
        #p4 = sympy.Symbol('p4')
        #p4 = 0
        p2 = sympy.Symbol('p2')
        p3 = sympy.Symbol('p3')
        
        
        gamma = sympy.Symbol('gamma')
        beta = sympy.Symbol('beta')
        
        eq1 = mu*p1 +eta*p2 - 1 - beta*RE - gamma
        eq2 = eta*p1 + mu*p2 -1 - beta*TE -gamma
        eq3 = mu*p3 + eta*p4 - beta*SE - gamma
        eq4 = eta*p3 + mu*p4 - beta*PE -gamma
        eq5 = beta*sy + gamma
        ans = sympy.solve([eq1,eq2,eq3,eq4,eq5])
        #if 1>=ans[p1] >=0 and 1>= ans[p2] >=0 and 1>= ans[p3] >=0:
        #if 1>=ans[p1] >=0 and 1>= ans[p2] >=0 and 1>= ans[p3] >=0:
        print(ans)
        print(ans[p1],ans[p2])#,ans[sy])
"""

Solve[{
  		mu*p1 + eta*p2 - 1 == beta*RE + gamma,
                    eta*p1 + mu*p2 - 1 == beta*TE + gamma,
  		mu*p3 + eta*p4 == beta + SE + gamma,
  		eta*p3 + mu*p4 == beta*PE + gamma,
  		beta*sy + gamma == 0},
 {p2, p3, sy, beta, gamma}]

Solve[{mu*p1 + eta*p2 - 1 - beta*RE - gamma == 0, 
  eta*p1 + mu*p2 - 1 - beta*TE - gamma == 0, 
  mu*p3 + eta*p4 - beta*SE - gamma == 0, 
  eta*p3 + mu*p4 - beta*PE - gamma == 0, beta*sy + gamma == 0},
 {p2, p3, sy, beta, gamma}]"""